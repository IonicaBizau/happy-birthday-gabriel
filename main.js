var TextAnimation = require ("text-animation")
  , Overlap       = require ("overlap")
  , JAsciimo       = require('jasciimo').Figlet
  , util = require('util');
  ;

JAsciimo.write("Gabriel! :-)", "starwars", function(art){
    TextAnimation.start({
        animate: "down-up"
      , text: "Happy Birthday,"
      , delay: 100
      , size: "43x30"
      , end: function (frames) {
            util.print("\u001b[2J\u001b[0;0H");
            console.log(Overlap({
                who: frames[frames.length - 1]
              , with: art
              , where: {
                    x: 10
                  , y: 10
                }
            }));
        }
    });
});
